<?php
/*
Plugin Name: Church Registration Form
Plugin URI:  
Description: Registration form with wp_nonce, spam honey pot, error handling
Version:     1.0.0
*/


class CWRF_Form {
	public $form_name;
	public $method;
	public $submit_text;
	public $user_data;
	public $fields = array();
	public $message;


	function __construct( $form_name = '', $fields = array(), $submit_text = 'Submit' ) {
		$this->form_name   = $form_name;
		$this->fields      = $fields;
		$this->submit_text = $submit_text;
		$this->cwrf_build_form( $fields );
	}


	function cwrf_build_form( $fields ) {
		//build_form runs in construct with an empty $fields and runs again w/ a set $fields array
		//This stops build_form from firing if the $fields array is empty
		if ( empty( $fields ) ) {
			return;
		}

		?>

			<form name="<?php echo esc_attr( $this->form_name ) ?>" method="POST">
				<div class="form-title">
					<h2><?php echo esc_html( $this->form_name )?></h2>
				</div>

				<?php

				foreach( $fields as $input_fields ) {
					extract( $input_fields );
					if( $required === true ) {
						$req = 'required';
					} else {
						$req = '';
					}
					switch( $type ) {
						case 'text': 
							$input = sprintf( '<div class="%s"><p><input type="%s" name="%s" id="%s" minlength="%d" maxlength="%d" placeholder="%s" ' . $req . '></p></div>', 
								esc_attr( $class ), 
								esc_attr( $type ), 
								esc_attr( $name ), 
								esc_attr( $id ), 
								$minlength, 
								$maxlength, 
								esc_attr( $placeholder ) 
							);
							echo $input;
							break;

						case 'password': 
							$input = sprintf( '<div class="%s"><p><input type="%s" name="%s" id="%s" minlength="%d" maxlength="%d" placeholder="%s" ' . $req . '></p></div>', 
								esc_attr( $class ), 
								esc_attr( $type ), 
								esc_attr( $name ), 
								esc_attr( $id ), 
								$minlength, 
								$maxlength, 
								esc_attr( $placeholder )
							 );
							echo $input;
							break;

						case 'email':
							$input = sprintf( '<div class="%s"><p><input type="%s" name="%s" id="%s" minlength="%d" maxlength="%d" placeholder="%s" ' . $req . '></p></div>', 
								esc_attr( $class ), 
								esc_attr( $type ), 
								esc_attr( $name ), 
								esc_attr( $id ), 
								$minlength, 
								$maxlength, 
								esc_attr( $placeholder ) 
							);
							echo $input;
							break;

						case 'textarea':
							$input = sprintf( '<div class="%s"><p><textarea name="%s" id="%s" rows="%d" cols="%d" placeholder="%s" ' . $req . '></textarea></p></div>', 
								esc_attr( $class ), 
								esc_attr( $name ), 
								esc_attr( $id ), 
								$rows, 
								$cols, 
								esc_attr( $placeholder ) 
							);
							echo $input;
							break;

					}
				}

				$registration_token = strtolower( wp_generate_password( 10, false, false ) ) ?>

				<input type="hidden" name="registration_token" value="<?php echo esc_attr( $registration_token ) ?>">

				<input id="test" type="text" name="pot" value="" style="display:none;">

				<?php wp_nonce_field( 'user_registration-' . $registration_token, 'registration_nonce' ) ?>
					<p>
						<button type="submit" id="submit-registration"><?php echo esc_html( $this->submit_text ) ?></button>
					</p>
				</div>
			</form>

			<style>
				.registration-form.container {
					max-width: 700px;
					margin: 0 auto;
					padding: 0 20px;
				}
			</style>
		<?php
	}


	public function cwrf_process_form() {
		//Check to see if 'POST' exists and verify nonce.
		if ( 'POST' !== $_SERVER['REQUEST_METHOD']||! isset( $_POST['registration_token'] )||! isset( $_POST['registration_nonce'] )||false === wp_verify_nonce( $_POST['registration_nonce'], 'user_registration-' . $_POST['registration_token'] )) {
				return;
			}

		if ( ! empty( $_POST['pot'] ) ) {
			wp_die( 'Hello, Mr. Roboto. No form for you.' );
		}

		//Check to see if user is logged in
		if ( is_user_logged_in() ) {
			wp_die( 'You are already logged in as a user.' );
		} else {
			foreach ( $_POST as $key => $value ) {
				//Validate email if it exists
				if ( false !== is_email( $value ) && email_exists( $value ) !== false ) {
					wp_die( 'That email address is associated with another user.' );
				} 
				//Sanitize email
				if ( false !== is_email( $value ) ) {
					$clean_email = sanitize_email( $value );
					$user_data[ $key ] = $clean_email;
				//Make sure value exists and sanitize it
				} elseif ( $key !== 'pot' && empty( $value ) ) {
					wp_die( 'Please enter a value for the <i><b>' . ucwords( str_replace( '_', ' ', $key ) ) . '</b></i> field.' );
				} else {
					$clean_input = sanitize_text_field( $value );
					$user_data[$key] = $clean_input;
				}
			}
		}

		if ( ! empty( $user_data ) ) {
			extract( $user_data );

			//Create new WP user and return new ID
			$user_meta_fields = array_diff_key( $user_data, array( 
				'registration_nonce' => null, 
				'_wp_http_referer'   => null, 
				'registration_token' => null, 
				'pot'                => null ,

			) );

			$user_id = wp_insert_user( $user_meta_fields );

			//Add the rest of the fields to user meta in your WordPress db
			foreach ( $user_meta_fields as $meta_key => $meta_value ) {
				add_user_meta( $user_id, $meta_key, $meta_value );
			}
			
			// update user role to pending
			$u = new WP_User( $user_id );

			// Remove role
			$u->remove_role( 'subscriber' );
			
			// Add role
			$u->add_role( 'pending_application' );

			// wp_new_user_notification notify admin


			$creds = array(
				'user_login'    => $user_login,
				'user_password' => $user_pass,
				'remember'      => true,
			);

			$user = wp_signon( $creds, false );

			if ( is_wp_error( $user ) ) {
				wp_die( $user->get_error_message() );
			}

		} else {
			wp_die( "Required fields are missing. Please fill out form." );
		}

		wp_redirect( home_url() );
		exit;
	}

	/**
 	 * Show custom user meta in user admin panel
 	 *
     * @param object $user is WP_User object. Gets passed at hook 
     */
    public function cwrf_user_details( $user ) {

  		$all_meta = array_map( function( $a ){ return $a[0]; }, get_user_meta( $user->ID ) );

    	//meta_keys to exclude from adding to profile page
    	//because they have been added in the premade fields or aren't relevant
    	//The values in this array are irrelevant
    	$custom_meta = array_diff_key( $all_meta, array(
			'nickname'             => null, 
			'first_name'           => null, 
			'last_name'            => null, 
			'user_email'           => null, 
			'user_pass'            => null, 
			'user_login'           => null, 
			'test_user_level'      => null, 
			'test_capabilities'    => null, 
			'show_admin_bar_front' => null, 
			'use_ssl'              => null, 
			'admin_color'          => null, 
			'comment_shortcuts'    => null, 
			'rich_editing'         => null,
			'description'          => null,
    	 ) );

    	 ?>

    	<h3><?php echo $user->first_name ?>'s Details</h3>
    	<table class="form-table">
    	<?php 
	    	foreach ( $custom_meta as $key => $value ) {
	    		?>
	    		<tr>
	    			<th><label><?php echo esc_html( ucwords( str_replace( '_', ' ', $key ) ) ) ?></label></th>
	    			<td><input type="text" value="<?php echo $value ?>" class="regular-text"/></td>
	        	</tr>
	        	<?php
	        }
        ?>
        </table>
        <?php
	}	
}

$form = new CWRF_Form;

add_action( 'wp', array( $form, 'cwrf_process_form' ) );

// add_action( 'show_user_profile', array( $form, 'cwrf_user_details' ), 999 );
// add_action( 'edit_user_profile', array( $form, 'cwrf_user_details' ), 999 );

function activate_formplugin() {

	// create for user registration
	$check_page_exist = get_page_by_title('log-into-church', 'OBJECT', 'page');
	// Check if the page already exists
	if(empty($check_page_exist)) {
		$page_id = wp_insert_post(
			array(
			'comment_status' => 'close',
			'ping_status'    => 'close',
			'post_author'    => 1,
			'post_title'     => ucwords('log-into-church'),
			'post_name'      => strtolower(str_replace(' ', '-', trim('log-into-church'))),
			'post_status'    => 'publish',
			'post_content'   => 'Content of the page',
			'post_type'      => 'page',
			'post_parent'    => 'id_of_the_parent_page_if_it_available'
			)
		);
	}


	add_role('pending_application', 'Pending Application' ,array( 'read' => true ));
}


function deactivate_formplugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name-deactivator.php';
	Plugin_Name_Deactivator::deactivate();
}

// activate plugin
register_activation_hook( __FILE__, 'activate_formplugin' );
// register_deactivation_hook( __FILE__, 'deactivate_formplugin' );


// load template when page visited

function wpa3396_page_template( $page_template )
{
	if ( is_page( 'log-into-church' ) ) {
		$page_template = dirname( __FILE__ ) . '/page-log-into-church.php';
	}
	return $page_template;
}

add_filter( 'page_template', 'wpa3396_page_template' );

// load templte when page visited

// form template
function formTemplate( $page_template )
{
	if ( is_page( 'submit-content' ) ) {
		$page_template = dirname( __FILE__ ) . '/page-submitcontent.php';
	}
	return $page_template;
}

// add_filter( 'page_template', 'formTemplate' );




// add categories to wordpress media files
function wptp_add_categories_to_attachments() {
    register_taxonomy_for_object_type( 'category', 'attachment' );
}
add_action( 'init' , 'wptp_add_categories_to_attachments' );


// hook into media upload and create transient
add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

function custom_upload_filter( $file ){
		// set file to update later
		$file_parts = pathinfo( $file['name'] );
		set_transient( '_set_attachment_title', $file_parts['filename'], 30 );

    return $file;
}



// set media file category
add_action( 'add_attachment', '_set_attachment_title' );

function _set_attachment_title( $attachment_id ) {

	// check if transient was set
	$title = get_transient( '_set_attachment_title' );
	
    if ( $title ) {

        // update attachment title and caption
		// wp_update_post( array( 'ID' => $attachment_id, 'post_title' => $title, 'post_excerpt' => $title ) );
		
		// check current user capabilities
		$user = wp_get_current_user();
		if ( in_array( 'contributor', (array) $user->roles ) ) {
			// set category to pending for contributors
			$cat_update = wp_set_post_categories($attachment_id, array(2));

			// notify admin of media upload
		}

        // update other metadata
        // update_post_meta( $attachment_id, '_wp_attachment_image_alt', $title );

        // delete the transient for this upload
        delete_transient( '_set_attachment_title' );
    }
}


// limit contributor editing capabilities
