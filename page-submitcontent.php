<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			?><div class="registration-form container"><?php
			$fields = array(
				'First Name' => array(
					'name'        => 'first_name',
					'type'        => 'text',
					'id'          => 'first_name',
					'class'       => '',
					'minlength'   => 1,
					'maxlength'   => 50,
					'placeholder' => 'First Name',
					'required'    => true
				),
				'Last Name' => array(
					'name'        => 'last_name',
					'type'        => 'text',
					'id'          => 'last_name',
					'class'       => '',
					'minlength'   => 1,
					'maxlength'   => 50,
					'placeholder' => 'Last Name',
					'required'    => true
				),
				'Username' => array(
					'name'        => 'user_login',
					'type'        => 'text',
					'id'          => 'user_login',
					'class'       => '',
					'minlength'   => 1,
					'maxlength'   => 50,
					'placeholder' => 'User Login',
					'rows'        => '',
					'cols'        => '',
					'required'    => true
				),
				'Password' => array(
					'name'        => 'user_pass',
					'type'        => 'password',
					'id'          => 'user_pass',
					'class'       => '',
					'minlength'   => 1,
					'maxlength'   => 50,
					'placeholder' => 'Password',
					'rows'        => '',
					'cols'        => '',
					'required'    => true
				),
				'Email Address' => array(
					'name'        => 'user_email',
					'type'        => 'email',
					'id'          => 'user_email',
					'class'       => '',
					'minlength'   => 1,
					'maxlength'   => 50,
					'placeholder' => 'jon@mail.com',
					'required'    => false
				),
				'Options' => array(
					'name'        => 'favorite_fruit',
					'type'        => 'select',
					'id'          => 'favorite_fruit',
					'class'       => '',
					'options'     => array( 'apple', 'cherry', 'pear' ),
					'required'    => false
				),


			);
			if ( is_user_logged_in() ) {
				echo 'you are logged in';
			} else {
				$form = new CWRF_Form( 'Register to be a constributor!', $fields, 'Sign Up!' );
			}
			

			?></div><?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
