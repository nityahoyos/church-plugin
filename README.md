=== Custom WP Registration Form ===
Contributors: adamcarter
Tags: registration, registration form, secure, custom, WordPress plugin
Requires at least: 3.0.1
Tested up to: 4.4.1
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Description:
For the test, I created a plugin that creates a form page for users to register. Once users register, upon creating their role is set to Pending Approval which the admin has to approve and set to contributor. Any content uploaded by the contributor is set to have a Pending Approval category which the admin has to check and remove.